﻿using RabbitMQ.Client;

namespace RabbitMqPlayground.Extensions
{
    public static class IBasicPropertiesExtension
    {
        public static IDictionary<string, object> ToDictionary(this IBasicProperties basicProperties)
        {
            return new Dictionary<string, object>
            {
                ["AppId"] = basicProperties.AppId,
                ["ContentEncoding"] = basicProperties.ContentEncoding,
                ["ContentType"] = basicProperties.ContentType,
                ["CorrelationId"] = basicProperties.CorrelationId,
                ["MessageId"] = basicProperties.MessageId,
                ["Expiration"] = basicProperties.Expiration,
                ["Headers"] = basicProperties.Headers,
                ["Persistent"] = basicProperties.Persistent,
                ["Priority"] = basicProperties.Priority,
                ["ReplyTo"] = basicProperties.ReplyTo,
                ["ReplyToAddress"] = basicProperties.ReplyToAddress,
                ["Timestamp"] = basicProperties.Timestamp,
                ["Type"] = basicProperties.Type,
                ["UserId"] = basicProperties.UserId,
            };
        }
    }
}
