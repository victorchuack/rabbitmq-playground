﻿namespace RabbitMqPlayground
{
    public class QueueMessage
    {
        public string Message { get; set; }

        public ulong DeliveryTag { get; set; }

        public string? Topic { get; set; }

        public string? RoutingKey { get; set; }

        public IDictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();

    }
}
