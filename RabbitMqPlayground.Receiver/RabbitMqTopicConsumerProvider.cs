﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMqPlayground.Extensions;
using System.Text;

namespace RabbitMqPlayground
{
    public class RabbitMqTopicConsumerProvider : IQueueConsumerProvider
    {
        private readonly IConnection connection;
        private readonly string topicName;
        private readonly string queueName;
        private readonly string routingKey;
        private readonly ILogger? logger;

        public RabbitMqTopicConsumerProvider(IConnection connection, string topicName, string queueName, string routingKey, ILogger? logger)
        {
            this.connection = connection;
            this.topicName = topicName;
            this.queueName = queueName;
            this.routingKey = routingKey;
            this.logger = logger;
        }

        public async Task Start(Func<QueueMessage, Task> messageReceiveAction, CancellationToken cancellationToken)
        {
            var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: topicName, type: ExchangeType.Topic);
            channel.QueueDeclare(queue: queueName);
            channel.QueueBind(queue: queueName, exchange: topicName, routingKey: routingKey);
            channel.BasicQos(0, 1, false);

            var consumer = new AsyncEventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var routingKey = ea.RoutingKey;
                var properties = ea.BasicProperties.ToDictionary();

                await messageReceiveAction(new QueueMessage { Message = message, RoutingKey = routingKey, DeliveryTag = ea.DeliveryTag, Topic = ea.Exchange, Properties = properties });

                ((IModel?)model)?.BasicAck(ea.DeliveryTag, false);
            };
            logger?.LogInformation(
                channel.BasicConsume(queue: queueName,
                                     autoAck: false,
                                     consumer: consumer)
            );

            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(1000, cancellationToken);
            }

            channel.Close();
        }
    }
}
