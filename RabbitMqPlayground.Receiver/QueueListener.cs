using RabbitMQ.Client;

namespace RabbitMqPlayground
{
    public class QueueListener : BackgroundService
    {
        private readonly IQueueConsumerProvider queueProvider;
        private readonly ILogger<QueueListener> logger;

        public QueueListener(IQueueConsumerProvider queueProvider, ILogger<QueueListener> logger)
        {
            this.queueProvider = queueProvider;
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await queueProvider.Start(
                message => Task.Run(() =>
                {
                    logger.LogInformation("Received message {message}", message.Message);
                }),
                stoppingToken);
        }
    }
}
