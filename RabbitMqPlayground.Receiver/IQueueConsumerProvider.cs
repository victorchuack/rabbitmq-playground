﻿
namespace RabbitMqPlayground
{
    public interface IQueueConsumerProvider
    {
        Task Start(Func<QueueMessage, Task> messageReceiveAction, CancellationToken cancellationToken);
    }
}