﻿
namespace RabbitMqPlayground.Sender
{
    public interface IQueueProducerProvider
    {
        Task Send(QueueMessage message);
    }
}