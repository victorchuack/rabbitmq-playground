namespace RabbitMqPlayground.Sender
{
    public class Worker : BackgroundService
    {
        private readonly IQueueProducerProvider queueProvider;
        private readonly ILogger<Worker> _logger;

        public Worker(IQueueProducerProvider queueProvider, ILogger<Worker> logger)
        {
            this.queueProvider = queueProvider;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_logger.IsEnabled(LogLevel.Information))
                {
                    _logger.LogInformation("Sending a message to Topic: {topicName}", DateTimeOffset.Now);
                }
                await queueProvider.Send(new QueueMessage { Message = $"Message at {DateTime.UtcNow.ToString("R")}" });

                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
