﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMqPlayground.Extensions;
using System.Collections.Concurrent;
using System.Text;

namespace RabbitMqPlayground.Sender
{
    public class RabbitMqTopicProducerProvider : IQueueProducerProvider
    {
        private readonly IConnection connection;
        private readonly string topicName;
        private readonly string routingKey;
        private readonly ILogger? logger;

        public RabbitMqTopicProducerProvider(IConnection connection, string topicName, string routingKey, ILogger? logger)
        {
            this.connection = connection;
            this.topicName = topicName;
            this.routingKey = routingKey;
            this.logger = logger;
        }

        public async Task Send(QueueMessage message)
        {
            var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: topicName, type: ExchangeType.Topic);
            channel.ConfirmSelect();

            var outstandingConfirms = new ConcurrentDictionary<ulong, string>();

            void CleanOutstandingConfirms(ulong sequenceNumber, bool multiple)
            {
                if (multiple)
                {
                    var confirmed = outstandingConfirms.Where(k => k.Key <= sequenceNumber);
                    foreach (var entry in confirmed)
                        outstandingConfirms.TryRemove(entry.Key, out _);
                }
                else
                    outstandingConfirms.TryRemove(sequenceNumber, out _);
            }

            channel.BasicAcks += (sender, ea) => CleanOutstandingConfirms(ea.DeliveryTag, ea.Multiple);
            channel.BasicNacks += (sender, ea) =>
            {
                outstandingConfirms.TryGetValue(ea.DeliveryTag, out string? body);
                Console.WriteLine($"Message with body {body} has been nack-ed. Sequence number: {ea.DeliveryTag}, multiple: {ea.Multiple}");
                CleanOutstandingConfirms(ea.DeliveryTag, ea.Multiple);
            };

            channel.BasicPublish(
                exchange: topicName,
                routingKey: routingKey,
                basicProperties: null,
                body: Encoding.UTF8.GetBytes(message.Message)
            );
            channel.Close();
        }
    }
}
