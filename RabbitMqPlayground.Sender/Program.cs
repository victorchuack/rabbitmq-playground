﻿// See https://aka.ms/new-console-template for more information

using RabbitMQ.Client;
using RabbitMqPlayground.Sender;

var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHostedService<Worker>();
builder.Services.AddSingleton<IQueueProducerProvider, RabbitMqTopicProducerProvider>(serviceProvider =>
{
    var config = serviceProvider.GetRequiredService<IConfiguration>();
    var logger = serviceProvider.GetService<ILoggerFactory>()?.CreateLogger<RabbitMqTopicProducerProvider>();
    return new RabbitMqTopicProducerProvider(
        serviceProvider.GetRequiredService<IConnection>(),
        config.GetValue<string>("Infra:RabbitMQ:TopicProvider:Topic") ?? string.Empty,
        config.GetValue<string>("Infra:RabbitMQ:TopicProvider:RoutingKey") ?? string.Empty,
        logger
    );
});
builder.Services.AddSingleton<IConnection>(serviceProvider =>
{
    var config = serviceProvider.GetRequiredService<IConfiguration>();
    var connectionFactory = new ConnectionFactory
    {
        UserName = config.GetValue<string>("Infra:RabbitMQ:Connection:UserName"),
        Password = config.GetValue<string>("Infra:RabbitMQ:Connection:Password"),
        VirtualHost = config.GetValue<string>("Infra:RabbitMQ:Connection:VirtualHost"),
        HostName = config.GetValue<string>("Infra:RabbitMQ:Connection:HostName"),
        Port = config.GetValue<int>("Infra:RabbitMQ:Connection:Port"),
        DispatchConsumersAsync = true,
    };
    return connectionFactory.CreateConnection();
});

var host = builder.Build();
host.Run();
